local x, y = 0, -1
local xDir, yDir = -1, 1
local MAX_COLUMNS = -13
local MAX_ROWS = 13
local CHANNEL = 3333
local R_CHANNEL = 3333
local modem = peripheral.wrap("left")
modem.open(R_CHANNEL)

local function getTime()
  return "[" .. os.date("%R") .. "] " .. os.getComputerLabel() .. ": "
end

local function turn()
  if yDir == -1 then
    turtle.turnRight()
    turtle.dig()
    turtle.forward()
    turtle.turnRight()
  else
    turtle.turnLeft()
    turtle.dig()
    turtle.forward()
    turtle.turnLeft()
  end
  x = x + xDir
  yDir = -yDir
end

local function moveForward(returning)
  if turtle.detect() then
    turtle.dig()
  end
  turtle.forward()
  if not returning then
    y = y + yDir
  end
  return true
end

local function goHome()
  print("Returning...")
  modem.transmit(CHANNEL, R_CHANNEL, getTime() .. "Returning with sugar cane...")
  if yDir == -1 then
    turtle.turnLeft()
  else
    turtle.turnRight()
  end
  local prevx = x
  local prevy = y

  while x < 0 do 
    moveForward(true)
    x = x + 1
  end
  turtle.turnRight()
  while y > -1 do
    moveForward(true)
    y = y - 1
  end
  local count = 0
  for i = 1, 16 do
    turtle.select(i)
    local items = turtle.getItemCount()
    count = count + items
    turtle.dropDown()
  end
  turtle.select(1)
  turtle.turnRight()
  turtle.turnRight()
  print("Collected", count, "items.")
  modem.transmit(CHANNEL, R_CHANNEL, getTime() .. "Collected " .. count .. " sugar cane.")
end

local function collect()
  if turtle.getFuelLevel() < 100 then
    modem.transmit(CHANNEL, R_CHANNEL, getTime() .. "Fuel level too low...")
    local refueled = false
    while not refueled do
      local e = os.pullEvent("turtle_inventory")
      turtle.refuel()
      if turtle.getFuelLevel() > 1000 then
        refueled = true
      end
    end
  end
  modem.transmit(CHANNEL, R_CHANNEL, getTime() .. "Collecting sugar cane...")
  moveForward(false)
  local done = false
  local i = 0
  while i < -MAX_COLUMNS and not done do
      local j = 0
      while j < MAX_ROWS - 1 do
        moveForward(false)
        j = j + 1
      end
      if x <= MAX_COLUMNS + 1 then
        done = true
        goHome()
        break
      end
      turn()
      i = i + 1
  end
  modem.transmit(CHANNEL, R_CHANNEL, getTime() .. "Fuel level: " .. turtle.getFuelLevel())
  modem.transmit(CHANNEL, R_CHANNEL, getTime() .. "Ready to collect sugar cane...")
end

modem.transmit(CHANNEL, R_CHANNEL, getTime() .. "Ready to collect sugar cane...")
while true do
  local event, side, ch, ch2, message, d = os.pullEvent()
  if event == "redstone" then
    collect()
  elseif event == "modem_message" then
    if message == "sc" then
      collect()
    elseif message == "sc quit" then
      break
    end
  end
end