local x, y = 0, -1
local xDir, yDir = 1, 1
local MAX_COLUMNS = 18
local MAX_ROWS = 8

local function refuel()
  local fuel = turtle.getFuelLevel()
  local needed = (MAX_COLUMNS - x) * (MAX_ROWS) + (MAX_ROWS - y) + 1
  if needed < fuel then
    turtle.refuel()
    return true
  else
    goHome()
    return false
  end
end

local function turn()
  if yDir == 1 then
    turtle.turnRight()
    turtle.dig()
    turtle.forward()
    turtle.turnRight()
  else
    turtle.turnLeft()
    turtle.dig()
    turtle.forward()
    turtle.turnLeft()
  end
  x = x + xDir
  yDir = -yDir
end

local function moveForward(returning)
  if turtle.detect() then
    turtle.dig()
  end
  turtle.forward()
  if not returning then
    y = y + yDir
  end
  return true
end

local function goHome()
  print("Returning...")
  if yDir == 1 then
    turtle.turnLeft()
  else
    turtle.turnRight()
  end
  local prevx = x
  local prevy = y

  while x > 0 do 
    moveForward(true)
    x = x - 1
  end
  turtle.turnLeft()
  while y > -1 do
    moveForward(true)
    y = y - 1
  end
  local count = 0
  for i = 1, 16 do
    turtle.select(i)
    local items = turtle.getItemCount()
    count = count + items
    turtle.dropDown()
  end
  turtle.select(1)
  turtle.turnRight()
  turtle.turnRight()
  print("Collected", count, "items.")
end

moveForward(false)
local done = false
local i = 0
while i < MAX_COLUMNS and not done do
    local j = 0
    while j < MAX_ROWS do
      moveForward(false)
      j = j + 1
    end
    if x >= MAX_COLUMNS - 1 then
      done = true
      goHome()
      break
    end
    turn()
    i = i + 1
end
